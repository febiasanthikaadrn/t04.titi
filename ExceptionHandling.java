import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan" + "\n");

        boolean validInput = false;
        do {
            try {
                System.out.print("Masukkan pembilang : ");
                int pembilang = scanner.nextInt();

                System.out.print("Masukkan penyebut  : ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.print("Hasil pembagian    : " + hasil);

                validInput = true;
            } catch (InputMismatchException e) {
                System.out.println("Bilangan yang dimasukkan bukan bilangan bulat. Silahkan masukkan angka lain");
                scanner.nextLine();
                System.out.println();
            } catch (ArithmeticException e) {
                System.out.println("Bilangan penyebut adalah angka nol. Silahkan masukkan angka lain");
                scanner.nextLine();
                System.out.println();
            }

        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        if(penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak boleh nol");
        }
        return pembilang / penyebut;
    }
}
